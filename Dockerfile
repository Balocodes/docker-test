FROM node:latest

WORKDIR /usr/src/app

COPY package*.json ./
COPY index.js ./

RUN npm install

EXPOSE 3333

CMD ["npm", "start"]